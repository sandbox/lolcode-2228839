civicrm_event_invitations
-------------------------

This module integrates with CiviCRM and makes it easy to send a CiviCRM mass mailing containing details of upcoming events.

Extends CiviCRM to provide new features on the mass mailing page:

* A drop down list of events that inserts formatted event details into the mailing template when selected.
* A token for each event that provides the same formatted event details but which is evaluated at the time
  the mailing is sent. (Work in Progress)

Authors

Created by Lola Slade (lolcode).
Initial development sponsored by The Association of Public Sector Information Professionals.
Ongoing development and support sponsored by Freeform Solutions.
