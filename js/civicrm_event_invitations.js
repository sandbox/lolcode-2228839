/**
 * @file
 * The module's JavaScript file does an Ajax call the menu hook that reads the themed event invitation.
 * It also takes care of inserting the html result into the various editors that CiviCRM can use.
 */


cj(function($) {

  var events = CRM.civicrm_event_invitations.event_list;

  function get_event_details(element) {
    var event_details;
    var event_id = $('#' + element.id + ' :selected').val();
    var insert_type = (element.id.indexOf('text') == -1) ? 'html' : 'text';
    var url = '/civicrm_event_invitations/get_event_details/' + insert_type + '/' + event_id;
    $.ajax({
      url: url,
      success: function(data) {
        event_details = urldecode(data);
      },
      async:false
    });
    return event_details;
  }

  function urldecode(str) {
    return decodeURIComponent((str+'').replace(/\+/g, '%20'));
  }

  var keys = {'text' : 'text', 'html' : 'html'};
  for (var insert_type in keys) {
    var select_text = '<select class="civicrm_event_invitations_insert" name="civicrm_event_invitations_events_'
      + insert_type + '" id="civicrm_event_invitations_events_' + insert_type + '" />';

    var sel = $(select_text);
    for (var i in events) {
        sel.append($('<option value="' + i + '"/>').html(events[i]));
    }

    var s = $('<div id="civicrm_event_invitations_events_' + insert_type
      + '_wrapper"><div class="event_label"><label for="civicrm_event_invitations_events_'
      + insert_type + '">' + Drupal.t('Insert Event Information:') + '</label></div><div class="event_control"></div></div>');

    $('.event_control',s).html(sel);
    s.insertBefore($('#help' + insert_type));

    $('#civicrm_event_invitations_events_' + insert_type).change(function(){
      var token = get_event_details(this);
      if (this.id == "civicrm_event_invitations_events_text") {
        $("#text_message").replaceSelection(token);
      }
      else {
        var editor = CRM.civicrm_event_invitations.civi_editor;
        var html_message = ($("#edit-html-message-value").length > 0) ? "edit-html-message-value" : "html_message";
        if ( editor == "tinymce" ) {
          tinyMCE.execInstanceCommand('html_message',"mceInsertContent",false, token );
        }
        else if ( editor == "ckeditor" ) {
          oEditor = CKEDITOR.instances[html_message];
          oEditor.insertHtml(token.toString() );
        }
        else if ( editor == "drupalwysiwyg" ) {
          if (Drupal.wysiwyg.instances[html_message].insert) {
            Drupal.wysiwyg.instances[html_message].insert(token.toString() );
          }
          else {
            alert("Sorry, your editor doesn't support this function yet.");
          }
        }
        else {
          $("#"+ html_message).replaceSelection(token);
        }
      }
    });
  }
});



