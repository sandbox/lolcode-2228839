<?php
/**
 * @file
 * Default theme implementation to display a CiviCRM event invitation.
 * You will want to theme with inline formatting in mind since this is used for email delivery.
 * Since CiviCRM emails have an HTML version and a text version there are two sections.
 * In the text section use basic html to format your text. It will be processed using drupal_html_to_text
 *
 * Available variables:
 * - $error: Did any error occur retrieving the event details?
 *
 * - $event: (array) All the standard event fields from the CiviCRM API get Event call
 *      Plus extra fields. (See next)
 *      $event['information_url'] (string) Event information page url
 *      $event['register_url'] (string) Event registration page url
 *
 * - $address (array) Event Location
 *      May be empty.
 *      All the standard address fields from the CiviCRM API get Address call.
 *      Additional friendly formatting for states and provinces
 *      $address['country'] = Country ISO code
 *      $address['state_province'] = State/Province code
 *      $address['country_name'] = Full Country name
 *      $address['state_province_name'] = Full State/Province name
 *
 * - $render_type: (string) Render in text or html
 *
 */
?>
<php if($error) : ?>
  <?php print $error; ?><br />
<php else : ?>
<?php if($render_type=='html') : ?>
  <table class="event_information" style="width: 500px; border: 1px solid grey; background-color: white; border-radius: 8px; font-family: Arial, Helvetica, sans-serif;">
    <tr class="event_information_row">
      <td style="width: 20%; font-weight: bold; padding-left: 8px;">&nbsp;</td>
      <td style="font-weight: bold;">Event Information</td>
    </tr>
    <tr>
      <td style="width: 20%; font-weight: bold; padding-left: 8px;"><?php print t('Title'); ?>: </td>
      <td><a href="<?php print $event['information_url']; ?>"><?php print $event['title']; ?></a></td>
    </tr>
    <tr class="event_information_row">
      <td style="width: 20%; font-weight: bold; padding-left: 8px;"><?php print t('Summary'); ?>: </td>
      <td><?php print $event['summary']; ?></td>
    </tr>
    <tr class="event_information_row">
      <td style="width: 20%; font-weight: bold; padding-left: 8px;"><?php print t('Start Date'); ?>: </td>
      <td><?php print $event['event_start_date']; ?></td>
    </tr>
    <tr class="event_information_row">
      <td style="width: 20%; font-weight: bold; padding-left: 8px;"><?php print t('End Date'); ?>: </td>
      <td><?php print $event['event_end_date']; ?></td>
    </tr>
    <tr class="event_information_row">
      <td style="width: 20%; height: 100%; vertical-align: top; font-weight: bold; padding-left: 8px;"><?php print t('Location'); ?>: </td>
      <td>
        <?php if(!empty($event['loc_block_id'])) : ?>
          <?php print $address['street_address']; ?><br />
          <?php if(!empty($address['supplemental_address_1'])) : ?><?php print $address['supplemental_address_1']; ?><br /><?php endif; ?>
          <?php print $address['city']; ?>, <?php print $address['state_province']; ?> <?php print $address['postal_code']; ?><br />
          <?php print $address['country_name']; ?><br />
        <?php endif; ?>
      </td>
    </tr>
    <tr class="event_information_row">
      <td style="width: 20%; font-weight: bold; padding-left: 8px;"></td>
      <td><a href="<?php print $event['information_url']; ?>"><?php print t('More Information...'); ?></a></td>
    </tr>
  </table>
<?php else: ?>
  <div>
    * <?php print t('Title'); ?>: <?php print $event['title']; ?><br />
    * <?php print t('Summary'); ?>: <?php print $event['summary']; ?><br />
    * <?php print t('Start Date'); ?>: <?php print $event['event_start_date']; ?><br />
    * <?php print t('End Date'); ?>: <?php print $event['event_end_date']; ?><br />
    <?php if(!empty($event['loc_block_id'])) : ?>
      * <?php print t('Location'); ?>:<br />
      &nbsp;&nbsp;&nbsp;&nbsp;<?php print $address['street_address']; ?><br />
      <?php if(!empty($event['address']['supplemental_address_1'])) : ?>
        &nbsp;&nbsp;&nbsp;&nbsp;<?php if(!empty($address['supplemental_address_1'])) : ?><?php print $address['supplemental_address_1']; ?><br /><?php endif; ?>
      <?php endif; ?>
      &nbsp;&nbsp;&nbsp;&nbsp;<?php print $address['city']; ?>, <?php print $address['state_province']; ?> <?php print $address['postal_code']; ?><br />
      &nbsp;&nbsp;&nbsp;&nbsp;<?php print $address['country_name']; ?><br />
    <?php endif; ?>
    * <?php print t('More Information'); ?>: <?php print $event['information_url']; ?>
  </div>
<?php endif; ?>
<php endif; ?>

